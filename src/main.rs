mod day_1;
mod day_2;

fn main() {
    println!("Hello, advent of code 2020!");

    day_1::part1();
    day_1::part2();

    println!(); //space between days

    day_2::part1();
    day_2::part2();

    println!(); //space between days
}
